from goods.models import Products
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank, SearchHeadline
from django.db.models import Q


def q_search(query):
    if query.isdigit() and len(query) <= 5:
        return Products.objects.filter(id=int(query))

    vector = SearchVector('name', 'description')
    query = SearchQuery(query)
    result = Products.objects.annotate(rank=SearchRank(vector, query)).filter(rank__gt=0).order_by("-rank")

    result = result.annotate(
        headLine=SearchHeadline(
            "name",
            query,
            start_sel='<b><span style="background-color: yellow;">',
            stop_sel='</span></b>',
        )
    )

    result = result.annotate(
        bodyLine=SearchHeadline(
            "description", query,
            start_sel='<b><span style="background-color: yellow;">',
            stop_sel='</span></b>',
        )
    )

    return result
