from django.shortcuts import render
from django.http import HttpResponse
from goods.models import Categories

def index(request):

    context = {
        'title': "Home main",
        'content': "Магазин мебели HOME",
    }
    return render(request, 'main/index.html', context)

def about(request):
    context = {
        'title': "Aout us",
        'content': "About us",
        'text_on_page': "Text about Why is this store so cool and what a good product."
    }
    return render(request, 'main/about.html', context)